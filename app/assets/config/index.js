export const API_URL = 'http://127.0.0.1:8000/api/';
export const COMPANY_API = 'company/';
export const BANK_API = 'bank/';
export const COMMODITY_API = 'commodity/';
export const COMMODITY_API_CREATE = 'commodity/create';
export const COMMODITY_API_UPDATE = 'commodity/update/';
export const IMPORT_API = 'import/';
export const IMPORT_API_CREATE = 'import/create';
export const IMPORT_API_UPDATE = 'import/update/';

export const PAYMENT_API = 'payment/';
export const PAYMENT_API_CREATE = 'payment/create';
export const PAYMENT_API_UPDATE = 'payment/update/';
