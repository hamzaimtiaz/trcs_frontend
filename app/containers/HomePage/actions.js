import {
  FETCH_COMPANY,
  FETCH_COMPANY_SUCCESS,
  FETCH_COMPANY_FAILED,
  FETCH_BANK,
  FETCH_BANK_SUCCESS,
  FETCH_BANK_FAILED,
  FETCH_COMMODITY,
  FETCH_COMMODITY_SUCCESS,
  FETCH_COMMODITY_FAILED,
  FETCH_SEARCH,
  FETCH_SEARCH_SUCCESS,
  FETCH_SEARCH_FAILED,
  ADD_IMPORT,
  ADD_IMPORT_SUCCESS,
  ADD_IMPORT_FAILED,
  FETCH_PAYMENT_SEARCH,
  FETCH_PAYMENT_SEARCH_SUCCESS,
  FETCH_PAYMENT_SEARCH_FAILED,
  ADD_PAYMENT,
  ADD_PAYMENT_SUCCESS,
  ADD_PAYMENT_FAILED,
  FETCH_ALL_COMMODITY,
  FETCH_ALL_COMMODITY_SUCCESS,
  FETCH_ALL_COMMODITY_FAILED,
  ADD_COMPANY,
  ADD_COMMODITY,
  ADD_BANK,
  ADD_COMPANY_SUCCESS,
  ADD_COMPANY_FAILED,
  ADD_COMMODITY_FAILED,
  ADD_COMMODITY_SUCCESS,
  ADD_BANK_SUCCESS,
  ADD_BANK_FAILED,
} from './constants';

export function fetchCompany() {
  return {
    type: FETCH_COMPANY,
  };
}
export function companyLoaded(payload) {
  return {
    type: FETCH_COMPANY_SUCCESS,
    payload,
  };
}
export function companyFailed(error) {
  return {
    type: FETCH_COMPANY_FAILED,
    error,
  };
}

export function fetchBank() {
  return {
    type: FETCH_BANK,
  };
}
export function bankLoaded(payload) {
  return {
    type: FETCH_BANK_SUCCESS,
    payload,
  };
}
export function bankFailed(error) {
  return {
    type: FETCH_BANK_FAILED,
    error,
  };
}

export function fetchCommodity(company) {
  return {
    type: FETCH_COMMODITY,
    company,
  };
}
export function commodityLoaded(payload) {
  return {
    type: FETCH_COMMODITY_SUCCESS,
    payload,
  };
}
export function commodityFailed(error) {
  return {
    type: FETCH_COMMODITY_FAILED,
    error,
  };
}

export function fetchAllCommodity() {
  return {
    type: FETCH_ALL_COMMODITY,
    
  };
}
export function allCommodityLoaded(payload) {
  return {
    type: FETCH_ALL_COMMODITY_SUCCESS,
    payload,
  };
}
export function allCommodityFailed(error) {
  return {
    type: FETCH_ALL_COMMODITY_FAILED,
    error,
  };
}

export function fetchSearch(search) {
  return {
    type: FETCH_SEARCH,
    search,
  };
}
export function searchLoaded(payload) {
  return {
    type: FETCH_SEARCH_SUCCESS,
    payload,
  };
}
export function searchFailed(error) {
  return {
    type: FETCH_SEARCH_FAILED,
    error,
  };
}

export function addImport(data) {
  return {
    type: ADD_IMPORT,
    data,
  };
}
export function addImportLoaded(payload) {
  return {
    type: ADD_IMPORT_SUCCESS,
    payload,
  };
}
export function addImportFailed(error) {
  return {
    type: ADD_IMPORT_FAILED,
    error,
  };
}


// PAYMENTS ACTIONS

export function fetchPaymentSearch(search) {
  return {
    type: FETCH_PAYMENT_SEARCH,
    search,
  };
}
export function searchPaymentLoaded(payload) {
  return {
    type: FETCH_PAYMENT_SEARCH_SUCCESS,
    payload,
  };
}
export function searchPaymentFailed(error) {
  return {
    type: FETCH_PAYMENT_SEARCH_FAILED,
    error,
  };
}

export function addPayment(data) {
  return {
    type: ADD_PAYMENT,
    data,
  };
}
export function addPaymentLoaded(payload) {
  return {
    type: ADD_PAYMENT_SUCCESS,
    payload,
  };
}
export function addPaymentFailed(error) {
  return {
    type: ADD_PAYMENT_FAILED,
    error,
  };
}

// ADDING COMPANY COMMODITY BANK
export function addCompany(data) {
  return {
    type: ADD_COMPANY,
    data,
  };
}
export function addCompanyLoaded(payload) {
  return {
    type: ADD_COMPANY_SUCCESS,
    payload,
  };
}
export function addCompanyFailed(error) {
  return {
    type: ADD_COMPANY_FAILED,
    error,
  };
}

export function addCommodity(data) {
  return {
    type: ADD_COMMODITY,
    data,
  };
}
export function addCommodityLoaded(payload) {
  return {
    type: ADD_COMMODITY_SUCCESS,
    payload,
  };
}
export function addCommodityFailed(error) {
  return {
    type: ADD_COMMODITY_FAILED,
    error,
  };
}

export function addBank(data) {
  return {
    type: ADD_BANK,
    data,
  };
}
export function addBankLoaded(payload) {
  return {
    type: ADD_BANK_SUCCESS,
    payload,
  };
}
export function addBankFailed(error) {
  return {
    type: ADD_BANK_FAILED,
    error,
  };
}