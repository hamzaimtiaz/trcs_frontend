// import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import MainBody from 'components/mainPage';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import {
  makeSelectCompany,
  makeSelectBank,
  makeSelectCommodity,
  makeSelectSearch,
  makeSelectPaymentSearch,
  makeSelectAllCommodity,
  makeSelectAdded,
} from './selectors';
import {
  fetchCompany,
  fetchBank,
  fetchCommodity,
  fetchSearch,
  fetchAllCommodity,
  addImport,
  fetchPaymentSearch,
  addPayment,
  addCompany,
  addCommodity,
  addBank,
} from './actions';

const key = 'forms';

// eslint-disable-next-line react/prop-types
function homePage({
  dispatchLoadCompany,
  company,
  dispatchLoadBank,
  banks,
  dispatchLoadCommodity,
  commodity,
  dispatchLoadAllCommodity,
  allCommodity,
  dispatchLoadSearch,
  searchData,
  dispatchAddImport,
  dispatchLoadPaymentSearch,
  searchPaymentData,
  dispatchAddPayment,
  dispatchAddCommodity,
  dispatchAddCompany,
  dispatchAddBank,
  added,
}) {

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatchLoadCompany();
    dispatchLoadBank();
    dispatchLoadAllCommodity();
    dispatchLoadSearch(null);
    dispatchLoadPaymentSearch(null)
  }, []);

  const handleOnCompanySelected = company => {
    dispatchLoadCommodity(company);
  };

  const handleOnCompanySearch = (ev,parameter) => {
    
    dispatchLoadSearch(parameter);
  };

  const handleOnPaymentSearch = parameter => ev => {
    dispatchLoadPaymentSearch(parameter);
  };

  const handleOnAddImport = ev => {
    
    dispatchAddImport(ev);
  };

  const handleOnAddPayment = ev => {
   
    dispatchAddPayment(ev);
  };

  const handleOnAddCompany = ev => {
    
    dispatchAddCompany(ev);
  };

  const handleOnAddCommodity = ev => {
    
    dispatchAddCommodity(ev);
  };

  const handleOnAddBank = ev => {
    
    dispatchAddBank(ev);
  };

  return (
    <div className="homeStyle">
      <div>
        <MainBody
          company={company}
          banks={banks}
          handleOnCompanySelected={handleOnCompanySelected}
          commodity={commodity}
          handleOnCompanySearch={handleOnCompanySearch}
          searchData={searchData}
          addImport={handleOnAddImport}
          handleOnPaymentSearch={handleOnPaymentSearch}
          searchPaymentData={searchPaymentData}
          addPayment={handleOnAddPayment}
          allCommodity={allCommodity}
          addCompany={handleOnAddCompany}
          addCommodity={handleOnAddCommodity}
          addBank={handleOnAddBank}
          added={added}
        />
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  company: makeSelectCompany(),
  banks: makeSelectBank(),
  commodity: makeSelectCommodity(),
  allCommodity: makeSelectAllCommodity(),
  searchData: makeSelectSearch(),
  searchPaymentData: makeSelectPaymentSearch(),
  added: makeSelectAdded(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadCompany: () => dispatch(fetchCompany()),
  dispatchLoadBank: () => dispatch(fetchBank()),
  dispatchLoadAllCommodity: () => dispatch(fetchAllCommodity()),
  dispatchLoadCommodity: companyId => dispatch(fetchCommodity(companyId)),
  dispatchLoadSearch: data => dispatch(fetchSearch(data)),
  dispatchAddImport: data => dispatch(addImport(data)),
  dispatchLoadPaymentSearch: data => dispatch(fetchPaymentSearch(data)),
  dispatchAddPayment: data => dispatch(addPayment(data)),
  dispatchAddCompany: data => dispatch(addCompany(data)),
  dispatchAddCommodity: data => dispatch(addCommodity(data)),
  dispatchAddBank: data => dispatch(addBank(data)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(homePage);
