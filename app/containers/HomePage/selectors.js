import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectDesign = state => state.forms || initialState;

export const makeSelectCompany = () =>
  createSelector(
    makeSelectDesign,
    forms => forms.company,
  );

export const makeSelectBank = () =>
  createSelector(
    makeSelectDesign,
    forms => forms.bank,
  );

export const makeSelectCommodity = () =>
  createSelector(
    makeSelectDesign,
    forms => forms.commodity,
  );

export const makeSelectSearch = () =>
createSelector(
  makeSelectDesign,
  forms => forms.importSearch,
);

export const makeSelectPaymentSearch = () =>
createSelector(
  makeSelectDesign,
  forms => forms.paymentSearch,
);

export const makeSelectAllCommodity = () =>
createSelector(
  makeSelectDesign,
  forms => forms.allCommodity,
);

export const makeSelectAdded = () =>
createSelector(
  makeSelectDesign,
  forms => forms.added,
);
