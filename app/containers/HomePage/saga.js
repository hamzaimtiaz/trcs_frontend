import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';

import {
  API_URL,
  COMPANY_API,
  BANK_API,
  COMMODITY_API,
  IMPORT_API,
  IMPORT_API_CREATE,
  IMPORT_API_UPDATE,
  PAYMENT_API,
  PAYMENT_API_CREATE,
  PAYMENT_API_UPDATE,
  COMMODITY_API_CREATE,
  COMMODITY_API_UPDATE,
} from '../../assets/config';
import {
  FETCH_COMPANY,
  FETCH_BANK,
  FETCH_COMMODITY,
  FETCH_ALL_COMMODITY,
  FETCH_SEARCH,
  ADD_IMPORT,
  ADD_COMPANY,
  ADD_COMMODITY,
  ADD_BANK,
  FETCH_PAYMENT_SEARCH,
  ADD_PAYMENT,
} from './constants';

import {
  companyLoaded,
  companyFailed,
  bankLoaded,
  bankFailed,
  commodityLoaded,
  commodityFailed,
  searchLoaded,
  searchFailed,
  addImportLoaded,
  addImportFailed,
  searchPaymentLoaded,
  searchPaymentFailed,
  addPaymentLoaded,
  addPaymentFailed,
  allCommodityLoaded,
  allCommodityFailed,
  addCompanyLoaded,
  addCompanyFailed,
  addCommodityLoaded,
  addCommodityFailed,
  addBankLoaded,
  addBankFailed,
  fetchCompany,
  fetchBank,
  fetchAllCommodity,
  fetchSearch,
  fetchPaymentSearch,
} from './actions';

// Getting Compay Data

export function* fetchCompanyData() {
  // var access_token = JSON.parse(localStorage.getItem('authentication')).access
  const requestURL = `${API_URL}${COMPANY_API}`;
  console.log('requestURL', requestURL);

  const companyGet = {
    method: 'GET',
    // headers: new Headers({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + access_token
    // })
  };
  try {
    const companyResponse = yield call(request, requestURL, companyGet);
    yield put(companyLoaded(companyResponse));
  } catch (err) {
    console.log('ERROR', err);
    yield put(companyFailed(err));
  }
}

// Getting Banks Data

export function* fetchBankData() {
  // var access_token = JSON.parse(localStorage.getItem('authentication')).access
  const requestURL = `${API_URL}${BANK_API}`;
  console.log('requestURL', requestURL);

  const bankGet = {
    method: 'GET',
    // headers: new Headers({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + access_token
    // })
  };
  try {
    const bankResponse = yield call(request, requestURL, bankGet);
    yield put(bankLoaded(bankResponse));
  } catch (err) {
    console.log('ERROR', err);
    yield put(bankFailed(err));
  }
}

// Getting Commodity Data

export function* fetchCommodityData(data) {
  // var access_token = JSON.parse(localStorage.getItem('authentication')).access
  const requestURL = `${API_URL}${COMMODITY_API}?company=${data.company}`;
  console.log('requestURL', requestURL);

  const commodityGet = {
    method: 'GET',
    // headers: new Headers({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + access_token
    // })
  };
  try {
    const commodityResponse = yield call(request, requestURL, commodityGet);
    yield put(commodityLoaded(commodityResponse));
  } catch (err) {
    console.log('ERROR', err);
    yield put(commodityFailed(err));
  }
}

// Getting all Commodity Data

export function* fetchAllCommodityData() {
  // var access_token = JSON.parse(localStorage.getItem('authentication')).access
  const requestURL = `${API_URL}${COMMODITY_API}`;
  console.log('requestURL', requestURL);

  const commodityGet = {
    method: 'GET',
    // headers: new Headers({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + access_token
    // })
  };
  try {
    const commodityResponse = yield call(request, requestURL, commodityGet);
    yield put(allCommodityLoaded(commodityResponse));
  } catch (err) {
    console.log('ERROR', err);
    yield put(allCommodityFailed(err));
  }
}

// IMPORT FUNCTONS

export function* fetchSearchData(data) {
  let company_id = null;
  let commodity_id = null;
  let bank_id = null;
  let transaction_type_id = null;
  let start_date_value = null;
  let end_date_value = null;
  let BL_number = null;

  if (data.search) {
    if (data.search.company_id == 0) {
      company_id = '';
    } else {
      company_id = data.search.company_id;
    }

    if (data.search.commodity_id == 0) {
      commodity_id = '';
    } else {
      commodity_id = data.search.commodity_id;
    }

    if (data.search.bank_id == 0) {
      bank_id = '';
    } else {
      bank_id = data.search.bank_id;
    }

    if (data.search.transaction_type_id == 0) {
      transaction_type_id = '';
    } else {
      transaction_type_id = data.search.transaction_type_id;
    }
    if (data.search.BL_number == null) {
      BL_number = '';
    } else {
      BL_number = data.search.BL_number;
    }

    if (data.search.start_date_value == null) {
      start_date_value = '';
    } else {
      start_date_value = `${data.search.start_date_value.getFullYear()}-${
        data.search.start_date_value.getMonth() + 1 < 10 ? '0' : ''
      }${data.search.start_date_value.getMonth() + 1}-${
        data.search.start_date_value.getDate() < 10 ? '0' : ''
      }${data.search.start_date_value.getDate()}`;
    }

    if (data.search.end_date_value == null) {
      end_date_value = '';
    } else {
      end_date_value = `${data.search.end_date_value.getFullYear()}-${
        data.search.end_date_value.getMonth() + 1 < 10 ? '0' : ''
      }${data.search.end_date_value.getMonth() + 1}-${
        data.search.end_date_value.getDate() < 10 ? '0' : ''
      }${data.search.end_date_value.getDate()}`;
    }

  } else {
    // date_value = `${new Date().getFullYear()}-${
    //   new Date().getMonth() + 1 < 10 ? '0' : ''
    // }${new Date().getMonth() + 1}-${
    //   new Date().getDate() < 10 ? '0' : ''
    // }${new Date().getDate()}`;
    start_date_value = '2020-07-02';
    end_date_value = '2020-07-30';
    company_id='';
    commodity_id='';
    transaction_type_id='';
    bank_id='';
    BL_number='';
  }
  const requestURL = `${API_URL}${IMPORT_API}?company=${company_id}&commodity=${commodity_id}&transaction_type=${transaction_type_id}&bank=${bank_id}&start_date=${start_date_value}&end_date=${end_date_value}&BL_number=${BL_number}`;
  console.log('requestURL', requestURL);

  const searchGet = {
    method: 'GET',
    // headers: new Headers({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + access_token
    // })
  };
  try {
    const commodityResponse = yield call(request, requestURL, searchGet);
    yield put(searchLoaded(commodityResponse));
  } catch (err) {
    console.log('ERROR', err);
    yield put(searchFailed(err));
  }
}

export function* addImportData(data) {
  if (data.data.type === 'Add') {
    const requestURL = `${API_URL}${IMPORT_API_CREATE}`;
    console.log('requestURL', requestURL);

    const importPOST = {
      method: 'POST',
      body: JSON.stringify(data.data),
      headers: new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + access_token
      }),
    };
    try {
      const importAddResponse = yield call(request, requestURL, importPOST);
      console.log('ADD Import Data', importAddResponse);
      yield put(addImportLoaded(importAddResponse));
      yield put(fetchSearch(null));

    } catch (err) {
      console.log('ERROR', err);
      yield put(addImportFailed(err));
    }
  } else if (data.data.type === 'Update') {
    const requestURL = `${API_URL}${IMPORT_API_UPDATE}${data.data.id}/`;
    console.log('data', data.data);

    const importPUT = {
      method: 'PUT',
      body: JSON.stringify(data.data),
      headers: new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + access_token
      }),
    };
    try {
      const importUpdateResponse = yield call(request, requestURL, importPUT);
      console.log('Update Import Data', importUpdateResponse);
      yield put(addImportLoaded(importUpdateResponse));
      yield put(fetchSearch(null));
    } catch (err) {
      console.log('ERROR', err);
      yield put(addImportFailed(err));
    }
  }
}

// PAYMENTS FUNCTIONS

export function* fetchPaymentSearchData(data) {
  // var access_token = JSON.parse(localStorage.getItem('authentication')).access
  let company_id = null;
  // let commodity_id = null;
  let bank_id = null;
  let payment_type_id = null;
  let start_date_value = null;
  let end_date_value = null;

  if (data.search) {
    if (data.search.company_id == 0) {
      company_id = '';
    } else {
      company_id = data.search.company_id;
    }

    if (data.search.bank_id == 0) {
      bank_id = '';
    } else {
      bank_id = data.search.bank_id;
    }

    if (data.search.payment_type_id == 0) {
      payment_type_id = '';
    } else {
      payment_type_id = data.search.payment_choice_id;
    }

    if (data.search.start_date_value == null) {
      start_date_value = '';
    } else {
      start_date_value = `${data.search.start_date_value.getFullYear()}-${
        data.search.start_date_value.getMonth() + 1 < 10 ? '0' : ''
      }${data.search.start_date_value.getMonth() + 1}-${
        data.search.start_date_value.getDate() < 10 ? '0' : ''
      }${data.search.start_date_value.getDate()}`;
    }

    if (data.search.end_date_value == null) {
      end_date_value = '';
    } else {
      end_date_value = `${data.search.end_date_value.getFullYear()}-${
        data.search.end_date_value.getMonth() + 1 < 10 ? '0' : ''
      }${data.search.end_date_value.getMonth() + 1}-${
        data.search.end_date_value.getDate() < 10 ? '0' : ''
      }${data.search.end_date_value.getDate()}`;
    }
  }
  else{
    // date_value = `${new Date().getFullYear()}-${
    //   new Date().getMonth() + 1 < 10 ? '0' : ''
    // }${new Date().getMonth() + 1}-${
    //   new Date().getDate() < 10 ? '0' : ''
    // }${new Date().getDate()}`;
    start_date_value = '2020-07-02';
    end_date_value = '2020-07-07';
    company_id='';
    payment_type_id='';
    bank_id='';
  }
  const requestURL = `${API_URL}${PAYMENT_API}?company=${company_id}&payment_type=${payment_type_id}&bank=${bank_id}&start_date=${start_date_value}&end_date=${end_date_value}`;
  console.log('requestURL', requestURL);

  const searchGet = {
    method: 'GET',
    // headers: new Headers({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + access_token
    // })
  };
  try {
    const paymentSearchResponse = yield call(request, requestURL, searchGet);
    yield put(searchPaymentLoaded(paymentSearchResponse));
    
  } catch (err) {
    console.log('ERROR', err);
    yield put(searchPaymentFailed(err));
  }
}

// ADD PAYMENT DATA
export function* addPaymentData(data) {
  // var access_token = JSON.parse(localStorage.getItem('authentication')).access
  if (data.data.type === 'Add') {
    const requestURL = `${API_URL}${PAYMENT_API_CREATE}`;
    console.log('requestURL', requestURL);

    const importPOST = {
      method: 'POST',
      body: JSON.stringify(data.data),
      headers: new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + access_token
      }),
    };
    try {
      const paymentAddResponse = yield call(request, requestURL, importPOST);
      console.log('ADD Payment Data', paymentAddResponse);
      yield put(addPaymentLoaded(paymentAddResponse));
      yield put(fetchPaymentSearch(null));
    } catch (err) {
      console.log('ERROR', err);
      yield put(addPaymentFailed(err));
    }
  } else if (data.data.type === 'Update') {
    const requestURL = `${API_URL}${PAYMENT_API_UPDATE}${data.data.id}/`;
    console.log('requestURL', requestURL);

    const paymentPUT = {
      method: 'PUT',
      body: JSON.stringify(data.data),
      headers: new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + access_token
      }),
    };
    try {
      const paymentUpdateResponse = yield call(request, requestURL, paymentPUT);
      console.log('Update Payment Data', paymentUpdateResponse);
      yield put(addPaymentLoaded(paymentUpdateResponse));
      yield put(fetchPaymentSearch(null));
    } catch (err) {
      console.log('ERROR', err);
      yield put(addPaymentFailed(err));
    }
  }
}

// ADD COMPANY DATA
export function* addCompanyData(data) {
  // var access_token = JSON.parse(localStorage.getItem('authentication')).access
  if (data.data.type === 'Add') {
    const requestURL = `${API_URL}${COMPANY_API}`;
    console.log('requestURL', requestURL);

    const companyPOST = {
      method: 'POST',
      body: JSON.stringify(data.data),
      headers: new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + access_token
      }),
    };
    try {
      const companyAddResponse = yield call(request, requestURL, companyPOST);
      console.log('ADD company Data', companyAddResponse);
      yield put(addCompanyLoaded(companyAddResponse));
      yield put(fetchCompany());
    } catch (err) {
      console.log('ERROR', err);
      yield put(addCompanyFailed(err));
    }
  } else if (data.data.type === 'Update') {
    const requestURL = `${API_URL}${COMPANY_API}${data.data.id}/`;
    console.log('requestURL', requestURL);

    const companyPUT = {
      method: 'PUT',
      body: JSON.stringify(data.data),
      headers: new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + access_token
      }),
    };
    try {
      const companyUpdateResponse = yield call(request, requestURL, companyPUT);
      console.log('Update Company Data', companyUpdateResponse);
      yield put(addCompanyLoaded(companyUpdateResponse));
      yield put(fetchCompany());
    } catch (err) {
      console.log('ERROR', err);
      yield put(addCompanyFailed(err));
    }
  }
}

// ADD COMMODITY DATA
export function* addCommodityData(data) {
  // var access_token = JSON.parse(localStorage.getItem('authentication')).access
  if (data.data.type === 'Add') {
    const requestURL = `${API_URL}${COMMODITY_API_CREATE}`;
    console.log('Data', data);

    const commodityPOST = {
      method: 'POST',
      body: JSON.stringify(data.data),
      headers: new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + access_token
      }),
    };
    try {
      const commodityAddResponse = yield call(
        request,
        requestURL,
        commodityPOST,
      );
      console.log('ADD commodity Data', commodityAddResponse);
      yield put(addCommodityLoaded(commodityAddResponse));
      yield put(fetchAllCommodity());
    } catch (err) {
      console.log('ERROR', err);
      yield put(addCommodityFailed(err));
    }
  } else if (data.data.type === 'Update') {
    const requestURL = `${API_URL}${COMMODITY_API_UPDATE}${data.data.id}/`;
    console.log('requestURL', requestURL);

    const commodityPUT = {
      method: 'PUT',
      body: JSON.stringify(data.data),
      headers: new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + access_token
      }),
    };
    try {
      const commodityUpdateResponse = yield call(
        request,
        requestURL,
        commodityPUT,
      );
      console.log('Update Company Data', commodityUpdateResponse);
      yield put(addCommodityLoaded(commodityUpdateResponse));
      yield put(fetchAllCommodity());
    } catch (err) {
      console.log('ERROR', err);
      yield put(addCommodityLoaded(err));
    }
  }
}

// ADD BANK DATA
export function* addBankData(data) {
  // var access_token = JSON.parse(localStorage.getItem('authentication')).access
  if (data.data.type === 'Add') {
    const requestURL = `${API_URL}${BANK_API}`;
    console.log('requestURL', requestURL);

    const bankPOST = {
      method: 'POST',
      body: JSON.stringify(data.data),
      headers: new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + access_token
      }),
    };
    try {
      const bankAddResponse = yield call(request, requestURL, bankPOST);
      console.log('ADD bank Data', bankAddResponse);
      yield put(addBankLoaded(bankAddResponse));
      yield put(fetchBank());
    } catch (err) {
      console.log('ERROR', err);
      yield put(addBankFailed(err));
    }
  } else if (data.data.type === 'Update') {
    const requestURL = `${API_URL}${BANK_API}${data.data.id}/`;
    console.log('requestURL', requestURL);

    const bankPUT = {
      method: 'PUT',
      body: JSON.stringify(data.data),
      headers: new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + access_token
      }),
    };
    try {
      const bankUpdateResponse = yield call(request, requestURL, bankPUT);
      console.log('Update Bank Data', bankUpdateResponse);
      yield put(addBankLoaded(bankUpdateResponse));
      yield put(fetchBank());
    } catch (err) {
      console.log('ERROR', err);
      yield put(addBankLoaded(err));
    }
  }
}

export default function* inventorySaga() {
  yield takeLatest(FETCH_COMPANY, fetchCompanyData);
  yield takeLatest(FETCH_BANK, fetchBankData);
  yield takeLatest(FETCH_COMMODITY, fetchCommodityData);
  yield takeLatest(FETCH_ALL_COMMODITY, fetchAllCommodityData);
  yield takeLatest(FETCH_SEARCH, fetchSearchData);
  yield takeLatest(ADD_IMPORT, addImportData);
  yield takeLatest(FETCH_PAYMENT_SEARCH, fetchPaymentSearchData);
  yield takeLatest(ADD_PAYMENT, addPaymentData);
  yield takeLatest(ADD_COMPANY, addCompanyData);
  yield takeLatest(ADD_COMMODITY, addCommodityData);
  yield takeLatest(ADD_BANK, addBankData);
}
