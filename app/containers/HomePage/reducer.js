import produce from 'immer';
import {
  FETCH_COMPANY_SUCCESS,
  FETCH_BANK_SUCCESS,
  FETCH_COMMODITY_SUCCESS,
  FETCH_SEARCH_SUCCESS,
  FETCH_PAYMENT_SEARCH_SUCCESS,
  FETCH_ALL_COMMODITY_SUCCESS,
  ADD_COMPANY_SUCCESS,
  ADD_PAYMENT_SUCCESS,
} from './constants';

export const initialState = {
  company: [],
  bank: [],
  commodity: [],
  allCommodity: [],
  importSearch: [],
  paymentSearch: [],
  added: null,
  isLoading: false,
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_COMPANY_SUCCESS:
        draft.company = action.payload;
        break;

      case FETCH_BANK_SUCCESS:
        draft.bank = action.payload;
        break;
      
      case FETCH_COMMODITY_SUCCESS:
        draft.commodity = action.payload;
        break;
      
      case FETCH_SEARCH_SUCCESS:
      draft.importSearch = action.payload;
      break;

      case FETCH_PAYMENT_SEARCH_SUCCESS:
      draft.paymentSearch = action.payload;
      break;

      case FETCH_ALL_COMMODITY_SUCCESS:
      draft.allCommodity = action.payload;
      break;

      case ADD_COMPANY_SUCCESS:
      draft.added = true;
      break;

      case ADD_PAYMENT_SUCCESS:
      draft.added = true;
      break;
    }
  });

export default homeReducer;
