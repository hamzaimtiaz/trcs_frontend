export const FETCH_COMPANY = 'FETCH_COMPANY';
export const FETCH_COMPANY_SUCCESS = 'FETCH_COMPANY_SUCCESS';
export const FETCH_COMPANY_FAILED = 'FETCH_COMPANY_FAILED';

export const FETCH_BANK = 'FETCH_BANK';
export const FETCH_BANK_SUCCESS = 'FETCH_BANK_SUCCESS';
export const FETCH_BANK_FAILED = 'FETCH_BANK_FAILED';

export const FETCH_COMMODITY = 'FETCH_COMMODITY';
export const FETCH_COMMODITY_SUCCESS = 'FETCH_COMMODITY_SUCCESS';
export const FETCH_COMMODITY_FAILED = 'FETCH_COMMODITY_FAILED';

export const FETCH_ALL_COMMODITY = 'FETCH_ALL_COMMODITY';
export const FETCH_ALL_COMMODITY_SUCCESS = 'FETCH_ALL_COMMODITY_SUCCESS';
export const FETCH_ALL_COMMODITY_FAILED = 'FETCH_ALL_COMMODITY_FAILED';

export const FETCH_SEARCH = 'FETCH_SEARCH';
export const FETCH_SEARCH_SUCCESS = 'FETCH_SEARCH_SUCCESS';
export const FETCH_SEARCH_FAILED = 'FETCH_SEARCH_FAILED';

export const ADD_IMPORT = 'ADD_IMPORT';
export const ADD_IMPORT_SUCCESS = 'ADD_IMPORT_SUCCESS';
export const ADD_IMPORT_FAILED = 'ADD_IMPORT_FAILED';

export const FETCH_PAYMENT_SEARCH = 'FETCH_PAYMENT_SEARCH';
export const FETCH_PAYMENT_SEARCH_SUCCESS = 'FETCH_PAYMENT_SEARCH_SUCCESS';
export const FETCH_PAYMENT_SEARCH_FAILED = 'FETCH_PAYMENT_SEARCH_FAILED';

export const ADD_PAYMENT = 'ADD_PAYMENT';
export const ADD_PAYMENT_SUCCESS = 'ADD_PAYMENT_SUCCESS';
export const ADD_PAYMENT_FAILED = 'ADD_PAYMENT_FAILED';

export const ADD_COMPANY = 'ADD_COMPANY';
export const ADD_COMPANY_SUCCESS = 'ADD_COMPANY_SUCCESS';
export const ADD_COMPANY_FAILED = 'ADD_COMPANY_FAILED';

export const ADD_COMMODITY = 'ADD_COMMODITY';
export const ADD_COMMODITY_SUCCESS = 'ADD_COMMODITY_SUCCESS';
export const ADD_COMMODITY_FAILED = 'ADD_COMMODITY_FAILED';

export const ADD_BANK = 'ADD_BANK';
export const ADD_BANK_SUCCESS = 'ADD_BANK_SUCCESS';
export const ADD_BANK_FAILED = 'ADD_BANK_FAILED';