import React, { useEffect, useState } from 'react';
import { Switch, Route } from 'react-router-dom';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

// import { makeSelectAuth, makeSelectCompany} from './selectors';
// import Login from 'containers/Login/Loadable';
// import Signup from 'containers/Signup/Loadable';
// import { fetchUserToken,fetchCompanySearch } from './actions';
// import emailDetail from '../EmailDetail';
// import fbAdDetail from '../FbAdDetail';
// import pageDetail from '../PageDetail';
// import companyDetail from '../CompanyDetail';
// import HomePage from '../HomePage';
// import Companies from '../Companies';

// import Header from 'components/header'
// import { select } from 'redux-saga/effects';

import { useInjectReducer } from '../../utils/injectReducer';
import reducer from './reducer';
import { useInjectSaga } from '../../utils/injectSaga';
import saga from './saga';
const key = 'app';

function App() {
  // authentication

  const routes = (
    <div id="content-wrap">
      <Switch>
        <Route path="/" component={HomePage} />
        <Route component={NotFoundPage} />
        {/* <Route path="/email-detail/" component={emailDetail} />
    <Route path="/fb-detail" component={fbAdDetail} />
    <Route path="/page-detail" component={pageDetail} />
    <Route path="/signup" component={Signup} />
    <Route path="/company" component={Companies} />
    <Route path="/company-detail" component={companyDetail} />
    <Route path="/login" component={Login} /> */}
      </Switch>
    </div>
  );

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    // dispatchFetchUserToken();
  }, []);

  // const [search, setSearch] = useState({search:""});

  // const handleOnSubmit = ev => {
  //   location.pathname =`/company/search=${search.search}`
  // };

  return <div className="mainStyle">{routes}</div>;

  // if (location.pathname.match(/login/) || location.pathname.match(/signup/)){

  // }
  // else{
  //   return (
  //     <div className="mainStyle">

  //       <div>
  // <Header
  // search = {search}
  // setSearch = {setSearch}
  // onButtonSubmit = {handleOnSubmit}/>
  // </div>
  //      {routes}
  //     </div>
  //   );
  // }
}

const mapStateToProps = createStructuredSelector({
  // authentication: makeSelectAuth(),
});

const mapDispatchToProps = dispatch => ({
  // dispatchFetchUserToken: data => dispatch(fetchUserToken(data)),
  // dispatchLoadCompanySearch: search => dispatch(fetchCompanySearch(search)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
