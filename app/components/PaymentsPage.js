import React, { useState, Fragment } from 'react';
import {
  Container,
  Tab,
  Tabs,
  Row,
  Col,
  Card,
  Button,
  Form,
  DropdownButton,
  Dropdown,
  Table,
  Modal,
  Alert,
} from 'react-bootstrap';

import Calendar from 'react-calendar';

import 'react-calendar/dist/Calendar.css';
import FadeIn from 'react-fade-in';

function PaymentBody({
  companies,
  commodities,
  banks,
  companySelectionHandler,
  handleOnPaymentSearch,
  searchPaymentData,
  addPayment,
}) {
  const [addshow, setAddShow] = useState(false);
  const [searches, setSearches] = useState({
    company: 'Company',
    company_id: 0,
    // commodity: 'Commodity',
    // commodity_id: 0,
    bank: 'Bank',
    payment_type: 'Transaction Type',
    payment_type_id: 0,
    bank_id: 0,
    start_date: 'Start Date',
    start_date_value: null,
    end_date: 'End Date',
    end_date_value: null,
  });

  const [payment, setPayment] = useState({
    id: 0,
    company: 0,
    // commodity: 0,
    bank: 0,
    company_name: 'Company',
    // commodity_name: 'Commodity',
    bank_name: 'Bank',
    payment_type: 0,
    payment_type_name: 'Payment Type',
    payment_choice_type: 0,
    payment_choice_type_name: 'Payment Medium',
    date: null,
    date_select: null,
    date_value: 'Date',
    container_no: null,
    type: 'Add',
  });

  let CompanyRows = <div> </div>;
  // let CommodityRows = <div> </div>;
  let BankRows = <div> </div>;
  let DataRows = <div />;

  const handleAddClose = () => setAddShow(false);
  const handleAddShow = () => setAddShow(true);

  const handleAddPayment = company_data => event => {
    addPayment(company_data);
    handleAddClose();
  };

  const handleEditPayment = data => event => {
    setPayment({
      ...payment,
      id: data.id,
      company: data.company,
      // commodity: data.commodity,
      bank: data.bank,
      company_name: 'Company',
      // commodity_name: 'Commodity',
      bank_name: 'Bank',
      
      payment_type: data.transaction_type,
      payment_type_name: 'Payment Type',

      payment_choice_type: data.transaction_type,
      payment_choice_type_name: 'Payment Medium',

      date: data.date,
      date_select: null,
      date_value: 'Date',

      container_no: data.container_no,
      amount: data.amount,
      
      type: 'Update',
    });
    handleAddShow();
  };

  const handleDropdownSelected = parameter => event => {
    if (parameter === 'StartDate') {
      setSearches({
        ...searches,
        start_date: event.toLocaleDateString(),
        start_date_value: event,
      });
    }
    if (parameter === 'EndDate') {
      setSearches({
        ...searches,
        end_date: event.toLocaleDateString(),
        end_date_value: event,
      });
    }
    if (parameter === 'Company') {
      // companySelectionHandler(event.split(',')[0]);
      setSearches({
        ...searches,
        company: event.split(',')[1],
        company_id: event.split(',')[0],
      });
    }
    // if (parameter === 'Commodity') {
    //   setSearches({
    //     ...searches,
    //     commodity: event.split(',')[1],
    //     commodity_id: event.split(',')[0],
    //   });
    // }
    if (parameter === 'Bank') {
      setSearches({
        ...searches,
        bank: event.split(',')[1],
        bank_id: event.split(',')[0],
      });
    }
    if (parameter === 'Payment') {
      setSearches({
        ...searches,
        payment_type: event.split(',')[1],
        payment_type_id: event.split(',')[0],
      });
    }
  };

  const handlePaymentValue = parameter => event => {
    if (parameter === 'company') {
      // companySelectionHandler(event.split(',')[0]);
      setPayment({
        ...payment,
        [parameter]: event.split(',')[0],
        company_name: event.split(',')[1],
      });
    } 
    // else if (parameter === 'commodity') {
    //   setCompany({
    //     ...company,
    //     [parameter]: event.split(',')[0],
    //     commodity_name: event.split(',')[1],
    //   });
    // }
     else if (parameter === 'bank') {
      setPayment({
        ...payment,
        [parameter]: event.split(',')[0],
        bank_name: event.split(',')[1],
      });
    } else if (parameter === 'payment_type') {
      setPayment({
        ...payment,
        [parameter]: event.split(',')[0],
        payment_type_name: event.split(',')[1],
      });
      
    } 
    else if (parameter === 'payment_choice_type') {
      setPayment({
        ...payment,
        [parameter]: event.split(',')[0],
        payment_choice_type_name: event.split(',')[1],
      });
      
    } else if (parameter === 'date') {
      setPayment({
        ...payment,
        [parameter]: `${event.getFullYear()}-${
          event.getMonth() + 1 < 10 ? '0' : ''
        }${event.getMonth() + 1}-${
          event.getDate() < 10 ? '0' : ''
        }${event.getDate()}`,
        date_value: event.toLocaleDateString(),
        date_select: event,
      });
    } else {
      setPayment({
        ...payment,
        [parameter]: event.target.value,
      });
    }
  };

  if (companies) {
    CompanyRows = companies.map(data => (
      <Dropdown.Item eventKey={new Array([data.id, data.name])}>
        {data.name}
      </Dropdown.Item>
    ));
  }
  
  if (banks) {
    BankRows = banks.map(data => (
      <Dropdown.Item eventKey={new Array([data.id, data.name])}>
        {data.name}
      </Dropdown.Item>
    ));
  }

  if (searchPaymentData.length>0) {
    const count = 0;
    DataRows = searchPaymentData.map((data, index) => (
      <tr>
        {/* <td>
          <Form.Check type="checkbox" />
        </td> */}
        <td>{index + 1}</td>
        <td>{data.company.name}</td>
        <td>{data.bank.name}</td>
        <td>{data.date}</td>
        <td>{data.container_no}</td>
        <td>{data.amount.toLocaleString()} PKR</td>
        <td>{data.payment_type}</td>
        <td>{data.payment_choice_type}</td>
        {data.prev_payment 
        ? <td>{data.prev_payment.toLocaleString()} PKR</td>
        : <td>0 PKR</td>
       }
        
        <td>{data.balance.toLocaleString()} PKR</td>
        <td>
          <Button variant="secondary" onClick={handleEditPayment(data)}>
            Edit
          </Button>
        </td>
      </tr>
    ));
  }
  else
  {
    DataRows = <Alert variant='secondary' dismissible >
      No data found!
      </Alert>
  }

  return (
    <div>
      <FadeIn delay="100">
        <Form>
          <div>
            <Card>
              <Card.Body>
                <Form.Group controlId="searchParams">
                  <Form>
                    <Row>
                      <Col xs={11}>
                        <div>
                          <Row>
                            <Col xs="auto">&nbsp;</Col>
                            <Col xs="auto">&nbsp;</Col>
                          </Row>
                        </div>

                        <div>
                          <Row>
                            
                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.start_date}
                                id="input-group-dropdown-1"
                              >
                                <Calendar
                                  onChange={handleDropdownSelected('StartDate')}
                                  value={searches.start_date_value}
                                />
                              </DropdownButton>
                            </Col>

                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.end_date}
                                id="input-group-dropdown-1"
                              >
                                <Calendar
                                  onChange={handleDropdownSelected('EndDate')}
                                  value={searches.end_date_value}
                                />
                              </DropdownButton>
                            </Col>

                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.company}
                                id="input-group-dropdown-1"
                                onSelect={handleDropdownSelected('Company')}
                              >
                                {CompanyRows}
                              </DropdownButton>
                            </Col>

                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.bank}
                                id="input-group-dropdown-1"
                                onSelect={handleDropdownSelected('Bank')}
                              >
                                {BankRows}
                              </DropdownButton>
                            </Col>

                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.payment_type}
                                id="input-group-dropdown-1"
                                onSelect={handleDropdownSelected('Payment')}
                              >
                                <Dropdown.Item eventKey="1,Received">
                                  Received
                                </Dropdown.Item>
                                <Dropdown.Item eventKey="2,Sent">
                                  Sent
                                </Dropdown.Item>
                              </DropdownButton>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col>
                        <Row>&nbsp;</Row>
                        <Row>
                          <Button
                            variant="secondary"
                            onClick={handleOnPaymentSearch(searches)}
                          >
                            Search
                          </Button>
                        </Row>
                      </Col>
                    </Row>
                  </Form>
                </Form.Group>
              </Card.Body>
            </Card>
          </div>
          &nbsp;
          <div>
            {' '}
            <Card>
              <Card.Header className="float-right">
                <Button
                  className="float-right"
                  variant="secondary"
                  style={{ margin: '5px' }}
                  onClick={handleAddShow}
                >
                  Add
                </Button>
                {''}
                &nbsp;
                <Button
                  className="float-right"
                  variant="secondary"
                  style={{ margin: '5px' }}
                >
                  Print
                </Button>{' '}
              </Card.Header>
              <Card.Body>
                <Container>
                  <Table
                    bordered
                    hover
                    size="xl"
                    class="table-responsive"
                    style={{
                      overflow: 'auto',
                      display: 'block',
                      'table-layout': 'auto',
                      'white-space': 'nowrap',
                      'height': '500px',
                      'overflow-y': 'scroll',
                    }}
                  >
                    <thead style={{position: 'sticky' , top: 0 ,'background-color': 'grey',color:'black'}}>
                      <tr>
                        {/* <th>&nbsp;</th> */}
                        <th>#</th>
                        <th>Company</th>

                        <th>Bank</th>

                        <th>Date</th>

                        <th>Container No</th>
                        <th>Amount</th>
                        <th>Payment Type</th>
                        <th>Payment Medium</th>
                        <th>Previous Payment</th>
                        <th>Balance</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>{DataRows}</tbody>
                  </Table>
                </Container>
              </Card.Body>
            </Card>
          </div>
        </Form>

        <Modal show={addshow} onHide={handleAddClose}>
          <Modal.Header closeButton>
            <Modal.Title>{payment.type} Payment</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              {/* <FadeIn delay="100"> */}
                
                  <Form.Group as={Row} controlId="formHorizontalCompany">
                    <Form.Label column>Company</Form.Label>
                    <Col>
                      <DropdownButton
                        variant="outline-secondary"
                        title={payment.company_name}
                        id="company"
                        onSelect={handlePaymentValue('company')}
                      >
                        {CompanyRows}
                      </DropdownButton>
                    </Col>
                  </Form.Group>

                  {/* <Form.Group as={Row} controlId="formHorizontalCompany">
                    <Form.Label column>Commodity</Form.Label>
                    <Col>
                      <DropdownButton
                        variant="outline-secondary"
                        title={company.commodity_name}
                        id="input-group-dropdown-1"
                        onSelect={handleCompanyValue('commodity')}
                      >
                        {CommodityRows}
                      </DropdownButton>
                    </Col>
                  </Form.Group> */}

                  <Form.Group as={Row} controlId="formHorizontalCompany">
                    <Form.Label column>Bank</Form.Label>
                    <Col>
                      <DropdownButton
                        variant="outline-secondary"
                        title={payment.bank_name}
                        id="input-group-dropdown-1"
                        drop="up"
                        onSelect={handlePaymentValue('bank')}
                      >
                        {BankRows}
                      </DropdownButton>
                    </Col>
                  </Form.Group>
                
                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Amount</Form.Label>
                    <Col>
                      <Form.Control
                        type="number"
                        placeholder="Amount"
                        value={payment.amount}
                        onChange={handlePaymentValue('amount')}
                      />
                    </Col>
                  </Form.Group>
                
                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Container Number</Form.Label>
                    <Col>
                      <Form.Control
                        type="number"
                        placeholder="Container Number"
                        value={payment.container_no}
                        onChange={handlePaymentValue('container_no')}
                      />
                    </Col>
                  </Form.Group>
                
                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Payment Type</Form.Label>
                    <Col>
                      <DropdownButton
                        variant="outline-secondary"
                        title={payment.payment_type_name}
                        id="input-group-dropdown-1"
                        drop="up"
                        onSelect={handlePaymentValue('payment_type')}
                      >
                        <Dropdown.Item eventKey="1,Received">Received</Dropdown.Item>
                        <Dropdown.Item eventKey="2,Sent">
                          Sent
                        </Dropdown.Item>
                      </DropdownButton>
                    </Col>
                  </Form.Group>
                
                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Payment Medium</Form.Label>
                    <Col>
                      <DropdownButton
                        variant="outline-secondary"
                        title={payment.payment_choice_type_name}
                        id="input-group-dropdown-1"
                        drop="up"
                        onSelect={handlePaymentValue('payment_choice_type')}
                      >
                        <Dropdown.Item eventKey="1,Debit">Debit</Dropdown.Item>
                        <Dropdown.Item eventKey="2,Credit">
                          Credit
                        </Dropdown.Item>
                        <Dropdown.Item eventKey="3,Cash">Cash</Dropdown.Item>
                      </DropdownButton>
                    </Col>
                  </Form.Group>
                
                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Date</Form.Label>
                    <Col>
                      <DropdownButton
                        variant="outline-secondary"
                        title={payment.date_value}
                        id="input-group-dropdown-1"
                        drop="up"
                      >
                        <Calendar
                          onChange={handlePaymentValue('date')}
                          value={payment.date_select}
                        />
                      </DropdownButton>
                    </Col>
                  </Form.Group>
                
              {/* </FadeIn> */}
            </Form>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={handleAddClose}>
              Close
            </Button>
            <Button
              id="companyAdd"
              variant="primary"
              onClick={handleAddPayment(payment)}
            >
              {payment.type} Payment
            </Button>
          </Modal.Footer>
        </Modal>
      </FadeIn>
    </div>
  );
}

export default PaymentBody;
