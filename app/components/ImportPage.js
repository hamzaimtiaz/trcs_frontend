import React, { useState,useRef, Fragment } from 'react';
import { useReactToPrint } from 'react-to-print';
import {
  Container,
  Tab,
  Tabs,
  Row,
  Col,
  Card,
  Button,
  Form,
  DropdownButton,
  Dropdown,
  Table,
  Modal,
  Alert,
} from 'react-bootstrap';

import Calendar from 'react-calendar';
// import CompanyPrint from './CompanyPrint';
import 'react-calendar/dist/Calendar.css';
import FadeIn from 'react-fade-in';

class CompanyPrint extends React.Component {
  
  renderTableData() {
    return this.props.searchData.map((data, index) => {
       return (
          <tr 
          style={{
            border: "none",
            height: 5,
          }}>
             <td>{index + 1}</td>
             <td>{data.company.name}</td>
        <td>{data.commodity.name}</td>
        
        <td>{data.price.toLocaleString()} PKR</td>
        <td>{data.weight.toLocaleString()} Kilo</td>
       
        <td>{data.date}</td>

        
        <td>{data.amount.toLocaleString()} PKR</td>
        {data.prev_balance
          ? <td>{data.prev_balance.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }

        <td>{data.balance.toLocaleString()} PKR</td>
          </tr>
       )
    })
 }

 
 
  render() {
    return (
      <div>
       <h1
       style={{ 
        //  'display': 'inline-block',
       'text-align': 'center',
       'vertical-align': 'middle',
      //  'margin-left': '500px',
       'font-family': 'Arial, Helvetica, sans-serif',
       'font-size':'25px',
       'font-style':'bold'}}>
      <img src={require("./logo.png")} width="150" height="150" alt=""/>
      𝔗𝔞𝔯𝔦𝔮 ℜ𝔢𝔥𝔪𝔞𝔫 ℭ𝔬𝔪𝔪𝔦𝔰𝔰𝔦𝔬𝔫 𝔖𝔥𝔬𝔭
      </h1>
      <Form>
        <Row>
          <Col xs="auto">
          <Form.Label>Email:</Form.Label>
       {/* <Form.Label>{this.props.searchData.company.name}</Form.Label> */}
          </Col>
          <Col xs="auto">
          <Form.Label>Email:</Form.Label>
       {/* <Form.Label>{this.props.searchData[0].company.name}</Form.Label> */}
          </Col>
        </Row>
      </Form>
        <Form>
          <div>

          <Table responsive='xl'
                    size="xs"
                    
                    style={{
                    //   overflow: 'auto',
                    //   display: 'block',
                      'table-layout': 'auto',
                      // 'white-space': 'nowrap',
                      'border-collapse': 'collapse',
                    //   height: '500px',
                    //   'overflow-y': 'scroll',
                    }}
                  >
                    <thead
                      style={{
                        // 'background-color': 'grey',
                        color: 'black',
                        'font-size': '14px',
                        'font-style':'bold'
                      }}
                    >
                      <tr
                      >
                        {/* <th>&nbsp;</th> */}
                        <th>#</th>
                        <th>Company</th>
                        <th>Commodity</th>
                        
                        <th>Price</th>
                        <th>Weight</th>
                       
                        <th>Date</th>
                        
                        <th>Amount</th>
                        <th>Previous Balance</th>
                        <th>Balance</th>

                      </tr>
                    </thead>
                    <tbody
                    style={{
                      // 'background-color': 'grey',
                      color: 'black',
                      'font-size': '12px',
                      // 'font-style':'bold',
                      'cell-spacing':'0',
                    }}>{this.renderTableData()}</tbody>
                  </Table>
           </div>
           </Form>
    </div>
      
    );
  }
}

function ImportBody({
  companies,
  commodities,
  banks,
  companySelectionHandler,
  handleOnCompanySearch,
  searchData,
  addImport,
}) {
  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  const [addshow, setAddShow] = useState(false);
  const [searches, setSearches] = useState({
    company: 'Company',
    company_id: 0,
    commodity: 'Commodity',
    commodity_id: 0,
    bank: 'Bank',
    transaction_type: 'Transaction Type',
    transaction_type_id: 0,
    bank_id: 0,
    start_date: 'Start Date',
    start_date_value: null,
    end_date: 'End Date',
    end_date_value: null,
    BL_number: null,
  });

  const [company, setCompany] = useState({
    id: 0,
    company: 0,
    commodity: 0,
    bank: 0,
    company_name: 'Company',
    commodity_name: 'Commodity',
    bank_name: 'Bank',
    transaction_type: 0,
    transaction_type_name: 'Transaction Type',
    
    BL_number: null,
    price: 0,
    cont: 0,
    date: null,
    date_select: null,
    date_value: 'Date',

    container_no1: null,
    container_no1_weight: 0,
    container_no2: null,
    container_no2_weight: 0,
    container_no3: null,
    container_no3_weight: 0,
    container_no4: null,
    container_no4_weight: 0,
    container_no5: null,
    container_no5_weight: 0,

    other_expense: 0,
    bank_charges: 0,
    clearing_charges: 0,
    dollar_rate: 0,

    transaction_number: 0,
    shipper: null,
    arrival_date: null,
    arrival_date_select: null,
    arrival_date_value: 'Arrival Date',

    weight: 0,
    contract_no:null,
    wh_tax:0,
    lc_commission:0,
    accountant_charges:0,
    insurance:0,
    broker_charges:0,

    type: 'Add',
  });

  let CompanyRows = <div> </div>;
  let CommodityRows = <div> </div>;
  let BankRows = <div> </div>;
  let DataRows = <div />;
  let extra_charges = <div></div>

  const handleAddClose = () => setAddShow(false);
  const handleAddShow = () => setAddShow(true);

  const handleAddImport = company_data => event => {
    addImport(company_data);
    handleAddClose();
  };


  const handleDateSelect = (ev, company_data) => {
    
    console.log('Searhc is', company_data);

    if (company.transaction_type == 1) {
      handleOnCompanySearch(ev, company_data);
      if (searchData.length == 1) {
        console.log('Inside company Edit', searchData);
        setCompany({
          ...company,
          container_no1: searchData[0].container_no1,
          container_no2: searchData[0].container_no2,
          container_no3: searchData[0].container_no3,
          container_no4: searchData[0].container_no4,
          container_no5: searchData[0].container_no5,
        });
      }
    }
  };

  const handleEditImport = data => event => {
    setCompany({
      ...company,
      id: data.id,
      company: data.company.id,
      commodity: data.commodity.id,
      bank: data.bank.id,
      company_name: data.company.name,
      commodity_name: data.commodity.name,
      bank_name: data.bank.name,
      transaction_type: data.transaction_type,
      transaction_type_name: 'Transaction Type',
      BL_number: data.BL_number,
      price: data.price,
      cont: data.cont,
      date: data.date,
      date_select: null,
      date_value: 'Date',
      container_no1: data.container_no1,
      container_no1_weight: data.container_no1_weight,
      container_no2: data.container_no1,
      container_no2_weight: data.container_no1_weight,
      container_no3: data.container_no1,
      container_no3_weight: data.container_no1_weight,
      container_no4: data.container_no1,
      container_no4_weight: data.container_no1_weight,
      container_no5: data.container_no1,
      container_no5_weight: data.container_no1_weight,

      other_expense: data.other_expense,
      bank_charges: data.bank_charges,
      clearing_charges: data.clearing_charges,
      dollar_rate: data.dollar_rate,

      transaction_number: data.transaction_number,
      shipper: data.shipper,
      arrival_date: data.arrival_date,
      arrival_date_select: null,
      arrival_date_value: 'Arrival Date',

      weight: data.weight,
      contract_no:data.contract_no,
      wh_tax:data.wh_tax,
      lc_commission:data.lc_commission,
      accountant_charges:data.accountant_charges,
      insurance:data.insurance,
      broker_charges:data.bank_charges,

      type: 'Update',
    });
    handleAddShow();
  };

  const handleDropdownSelected = parameter => event => {
    if (parameter === 'StartDate') {
      setSearches({
        ...searches,
        start_date: event.toLocaleDateString(),
        start_date_value: event,
      });
    }
    if (parameter === 'EndDate') {
      setSearches({
        ...searches,
        end_date: event.toLocaleDateString(),
        end_date_value: event,
      });
    }
    if (parameter === 'Company') {
      companySelectionHandler(event.split(',')[0]);
      setSearches({
        ...searches,
        company: event.split(',')[1],
        company_id: event.split(',')[0],
      });
    }
    if (parameter === 'Commodity') {
      setSearches({
        ...searches,
        commodity: event.split(',')[1],
        commodity_id: event.split(',')[0],
      });
    }
    if (parameter === 'Bank') {
      setSearches({
        ...searches,
        bank: event.split(',')[1],
        bank_id: event.split(',')[0],
      });
    }
    if (parameter === 'Transaction') {
      setSearches({
        ...searches,
        transaction_type: event.split(',')[1],
        transaction_type_id: event.split(',')[0],
      });
    }
  };

  const handleCompanyValue = parameter => event => {
    if (parameter === 'company') {
      companySelectionHandler(event.split(',')[0]);
      setCompany({
        ...company,
        [parameter]: event.split(',')[0],
        company_name: event.split(',')[1],
      });
    } else if (parameter === 'commodity') {
      setCompany({
        ...company,
        [parameter]: event.split(',')[0],
        commodity_name: event.split(',')[1],
      });
    } else if (parameter === 'bank') {
      setCompany({
        ...company,
        [parameter]: event.split(',')[0],
        bank_name: event.split(',')[1],
      });
    } else if (parameter === 'transaction_type') {
      setCompany({
        ...company,
        [parameter]: event.split(',')[0],
        transaction_type_name: event.split(',')[1],
      });

      if (event.split(',')[0] == 1) {
        setSearches({
          ...searches,
          transaction_type_id: 2,
        });
      }
    } else if (parameter === 'date') {
      setCompany({
        ...company,
        [parameter]: `${event.getFullYear()}-${
          event.getMonth() + 1 < 10 ? '0' : ''
        }${event.getMonth() + 1}-${
          event.getDate() < 10 ? '0' : ''
        }${event.getDate()}`,
        date_value: event.toLocaleDateString(),
        date_select: event,
      });
    } else if (parameter === 'arrival_date') {
      setCompany({
        ...company,
        [parameter]: `${event.getFullYear()}-${
          event.getMonth() + 1 < 10 ? '0' : ''
        }${event.getMonth() + 1}-${
          event.getDate() < 10 ? '0' : ''
        }${event.getDate()}`,
        arrival_date_value: event.toLocaleDateString(),
        arrival_date_select: event,
      });
    } else if (parameter === 'BL_number') {
      setCompany({
        ...company,
        [parameter]: event.target.value,
      });
      setSearches({
        ...searches,
        BL_number: event.target.value,
      });
    } else {
      setCompany({
        ...company,
        [parameter]: event.target.value,
      });
    }
  };

  if (companies) {
    CompanyRows = companies.map(data => (
      <Dropdown.Item eventKey={new Array([data.id, data.name])}>
        {data.name}
      </Dropdown.Item>
    ));
  }
  if (commodities) {
    CommodityRows = commodities.map(data => (
      <Dropdown.Item eventKey={new Array([data.id, data.name])}>
        {data.name}
      </Dropdown.Item>
    ));
  }
  if (banks) {
    BankRows = banks.map(data => (
      <Dropdown.Item eventKey={new Array([data.id, data.name])}>
        {data.name}
      </Dropdown.Item>
    ));
  }

  if (searchData.length > 0) {
    const count = 0;
    DataRows = searchData.map((data, index) => (
      <tr>
        {/* <td>
          <Form.Check type="checkbox" />
        </td> */}
        <td
        style={{
          position: 'sticky',
          // position: '-webkit-sticky',
          left:'0',
          'z-index':'2',
          // top: 0,
          'background-color': 'grey',
          color: 'white',
        }}
        >{data.BL_number}</td>
        <td
        >{index + 1}</td>
        
        <td>{data.company.name}</td>
        <td>{data.commodity.name}</td>
        <td>{data.bank.name}</td>
        <td>{data.transaction_type_name}</td>
        
        <td>{data.contract_no}</td>

        {data.transaction_type === 'Sale'
          ? <td>{data.price.toLocaleString()} PKR</td>
          : <td>{data.price.toLocaleString()} $</td>
        }

        {data.transaction_type === 'Sale'
          ? <td>{data.weight.toLocaleString()} Kilo</td>
          : <td>{data.weight.toLocaleString()} MT</td>
        }

        <td>{data.cont}</td>
        <td>{data.date}</td>

        {data.transaction_type === 'Sale'
          ? <td>{data.invoice.toLocaleString()} PKR</td>
          : <td>{data.invoice.toLocaleString()} $</td>
        }
        <td>{data.amount.toLocaleString()} PKR</td>
        {data.prev_balance
          ? <td>{data.prev_balance.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }

        <td>{data.balance.toLocaleString()} PKR</td>    
        
        {data.dollar_rate
          ? <td>{data.dollar_rate.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }

        {data.other_expense
          ? <td>{data.other_expense.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }
        {data.broker_charges
          ? <td>{data.broker_charges.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }
        {data.bank_charges
          ? <td>{data.bank_charges.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }
        {data.clearing_charges
          ? <td>{data.clearing_charges.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }
        {data.wh_tax
          ? <td>{data.wh_tax.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }
        {data.lc_commission
          ? <td>{data.lc_commission.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }
        {data.accountant_charges
          ? <td>{data.accountant_charges.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }
        {data.insurance
          ? <td>{data.insurance.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }
        
        

        <td>{data.transaction_number}</td>
        <td>{data.shipper}</td>
        <td>{data.arrival_date}</td>

        <td>{data.container_no1}</td>
        <td>{data.container_no1_weight.toLocaleString()}</td>
        <td>{data.container_no2}</td>
        <td>{data.container_no2_weight.toLocaleString()}</td>
        <td>{data.container_no3}</td>
        <td>{data.container_no3_weight.toLocaleString()}</td>
        <td>{data.container_no4}</td>
        <td>{data.container_no4_weight.toLocaleString()}</td>
        <td>{data.container_no5}</td>
        <td>{data.container_no5_weight.toLocaleString()}</td>

        
        <td>{data.container1_weight_difference.toLocaleString()}</td>
        <td>{data.container2_weight_difference.toLocaleString()}</td>
        <td>{data.container3_weight_difference.toLocaleString()}</td>
        <td>{data.container4_weight_difference.toLocaleString()}</td>
        <td>{data.container5_weight_difference.toLocaleString()}</td>
        
        <td>
          <Button variant="secondary" onClick={handleEditImport(data)}>
            Edit
          </Button>
        </td>
      </tr>
    ));
  } else {
    DataRows = (
      <Alert variant="secondary" dismissible>
        No data found!
    </Alert>
    );
  }

  if(company.transaction_type == 2)
  {
    extra_charges =
    <div>
              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Bank Expense</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Bank Expense"
                    value={company.bank_charges}
                    onChange={handleCompanyValue('bank_charges')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Clearing Charges</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Clearing Charges"
                    value={company.clearing_charges}
                    onChange={handleCompanyValue('clearing_charges')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>W/H Tax</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="W/H Tax"
                    value={company.wh_tax}
                    onChange={handleCompanyValue('wh_tax')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>LC Commission</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="LC Commission"
                    value={company.lc_commission}
                    onChange={handleCompanyValue('lc_commission')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Accountant Charges</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Accountant Charges"
                    value={company.accountant_charges}
                    onChange={handleCompanyValue('accountant_charges')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Insurance</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Insurance"
                    value={company.insurance}
                    onChange={handleCompanyValue('insurance')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Dollar Rate</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Dollar Rate"
                    value={company.dollar_rate}
                    onChange={handleCompanyValue('dollar_rate')}
                  />
                </Col>
              </Form.Group>

              

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Shipper</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Shipper"
                    value={company.shipper}
                    onChange={handleCompanyValue('shipper')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Arrival Date</Form.Label>
                <Col>
                  <DropdownButton
                    variant="outline-secondary"
                    title={company.arrival_date_value}
                    id="input-group-dropdown-1"
                    drop="up"
                  >
                    <Calendar
                      onChange={handleCompanyValue('arrival_date')}
                      value={company.arrival_date_select}
                    />
                  </DropdownButton>
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Other Expense</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Other Expense"
                    value={company.other_expense}
                    onChange={handleCompanyValue('other_expense')}
                  />
                </Col>
              </Form.Group>
              </div>
  }
  else if(company.transaction_type ==1)
  {
    extra_charges = 
    <div>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Broker Charges</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Broker Charges"
                    value={company.broker_charges}
                    onChange={handleCompanyValue('broker_charges')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Other Expense</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Other Expense"
                    value={company.other_expense}
                    onChange={handleCompanyValue('other_expense')}
                  />
                </Col>
              </Form.Group>

              </div>

  }

  return (
    <div>
    <div style={{ display: "none" }}>
      <CompanyPrint 
      ref={componentRef}
      searchData={searchData} />
      </div>
    <div >
      
      <FadeIn delay="100">
        <Form >
          <div>
            <Card>
              <Card.Body>
                <Form.Group controlId="searchParams">
                  <Form>
                    <Row>
                      <Col xs={11}>
                        <div>
                          <Row>
                            <Col xs="auto">&nbsp;</Col>
                            <Col xs="auto">&nbsp;</Col>
                          </Row>
                        </div>

                        <div>
                          <Row>
                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.start_date}
                                id="input-group-dropdown-1"
                              >
                                <Calendar
                                  onChange={handleDropdownSelected('StartDate')}
                                  value={searches.start_date_value}
                                />
                              </DropdownButton>
                            </Col>

                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.end_date}
                                id="input-group-dropdown-1"
                              >
                                <Calendar
                                  onChange={handleDropdownSelected('EndDate')}
                                  value={searches.end_date_value}
                                />
                              </DropdownButton>
                            </Col>

                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.company}
                                id="input-group-dropdown-1"
                                onSelect={handleDropdownSelected('Company')}
                              >
                                {CompanyRows}
                              </DropdownButton>
                            </Col>

                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.commodity}
                                id="input-group-dropdown-1"
                                onSelect={handleDropdownSelected('Commodity')}
                              >
                                {CommodityRows}
                              </DropdownButton>
                            </Col>

                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.bank}
                                id="input-group-dropdown-1"
                                onSelect={handleDropdownSelected('Bank')}
                              >
                                {BankRows}
                              </DropdownButton>
                            </Col>

                            <Col xs="auto">
                              <DropdownButton
                                variant="outline-secondary"
                                title={searches.transaction_type}
                                id="input-group-dropdown-1"
                                onSelect={handleDropdownSelected('Transaction')}
                              >
                                <Dropdown.Item eventKey="1,Sale">
                                  Sale
                                </Dropdown.Item>
                                <Dropdown.Item eventKey="2,Purchase">
                                  Purchase
                                </Dropdown.Item>
                              </DropdownButton>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col>
                        <Row>&nbsp;</Row>
                        <Row>
                          <Button
                            variant="secondary"
                            onClick={e => handleOnCompanySearch(e, searches)}
                          >
                            Search
                          </Button>
                        </Row>
                      </Col>
                    </Row>
                  </Form>
                </Form.Group>
              </Card.Body>
            </Card>
          </div>
          &nbsp;
          <div>
            {' '}
            <Card>
              <Card.Header className="float-right">
                <Button
                  className="float-right"
                  variant="secondary"
                  style={{ margin: '5px' }}
                  onClick={handleAddShow}
                >
                  Add
                </Button>
                {''}
                &nbsp;
                <Button
                  className="float-right"
                  variant="secondary"
                  style={{ margin: '5px' }}
                  onClick={handlePrint}
                >
                  Print
                </Button>{' '}
              </Card.Header>
              <Card.Body>
                <Container>
                  <Table
                    bordered
                    hover
                    size="xl"
                    class="table-responsive"
                    style={{
                      overflow: 'auto',
                      display: 'block',
                      'table-layout': 'auto',
                      'white-space': 'nowrap',
                      height: '500px',
                      'overflow-y': 'scroll',
                    }}
                  >
                    <thead
                      style={{
                        position: 'sticky',
                        top: 0,
                        'background-color': 'grey',
                        color: 'white',
                      }}
                    >
                      <tr>
                        {/* <th>&nbsp;</th> */}
                        <th>BL Number</th>
                        <th>#</th>
                        <th>Company</th>
                        <th>Commodity</th>
                        <th>Bank</th>
                        <th>Transaction Type</th>
                        {/* <th>BL Number</th> */}
                        <th>Contract Number</th>
                        <th>Price</th>
                        <th>Weight</th>
                        <th>Cont</th>
                        <th>Date</th>
                        
                        <th>Invoice</th>
                        <th>Amount</th>
                        <th>Previous Balance</th>
                        <th>Balance</th>
                        <th>Dollar Rate</th>

                        <th>Other Expense</th>
                        <th>Broker Charges</th>
                        <th>Bank Charges</th>
                        <th>Clearing Charges</th>
                        <th>W/H Tax</th>
                        <th>LC Commission</th>
                        <th>Accountant Charges</th>
                        <th>Commission</th>
                        

                        <th>Transaction Number</th>
                        <th>Shipper</th>
                        <th>Arrival Date</th>

                        

                        <th>Container 1</th>
                        <th>Container 1 Weight</th>
                        <th>Container 2</th>
                        <th>Container 2 Weight</th>
                        <th>Container 3</th>
                        <th>Container 3 Weight</th>
                        <th>Container 4</th>
                        <th>Container 4 Weight</th>
                        <th>Container 5</th>
                        <th>Container 5 Weight</th>

                        <th>C1 Wt diff</th>
                        <th>C2 Wt diff</th>
                        <th>C3 Wt diff</th>
                        <th>C4 Wt diff</th>
                        <th>C5 Wt diff</th>

                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>{DataRows}</tbody>
                  </Table>
                </Container>
              </Card.Body>
            </Card>
          </div>
        </Form>

        <Modal show={addshow} onHide={handleAddClose}>
          <Modal.Header closeButton>
            <Modal.Title>{company.type} Import</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group as={Row} controlId="formHorizontalCompany">
                <Form.Label column>Company</Form.Label>
                <Col>
                  <DropdownButton
                    variant="outline-secondary"
                    title={company.company_name}
                    id="company"
                    drop="up"
                    onSelect={handleCompanyValue('company')}
                  >
                    {CompanyRows}
                  </DropdownButton>
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCompany">
                <Form.Label column>Commodity</Form.Label>
                <Col>
                  <DropdownButton
                    variant="outline-secondary"
                    title={company.commodity_name}
                    id="input-group-dropdown-1"
                    drop="up"
                    onSelect={handleCompanyValue('commodity')}
                  >
                    {CommodityRows}
                  </DropdownButton>
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCompany">
                <Form.Label column>Bank</Form.Label>
                <Col>
                  <DropdownButton
                    variant="outline-secondary"
                    title={company.bank_name}
                    id="input-group-dropdown-1"
                    drop="up"
                    onSelect={handleCompanyValue('bank')}
                  >
                    {BankRows}
                  </DropdownButton>
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Transaction Type</Form.Label>
                <Col>
                  <DropdownButton
                    variant="outline-secondary"
                    title={company.transaction_type_name}
                    id="input-group-dropdown-1"
                    drop="up"
                    onSelect={handleCompanyValue('transaction_type')}
                  >
                    <Dropdown.Item eventKey="1,Sale">Sale</Dropdown.Item>
                    <Dropdown.Item eventKey="2,Purchase">
                      Purchase
                    </Dropdown.Item>
                  </DropdownButton>
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>BL Number</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="BL Number"
                    value={company.BL_number}
                    onChange={handleCompanyValue('BL_number')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Transaction Number</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Transaction Number"
                    value={company.transaction_number}
                    onChange={handleCompanyValue('transaction_number')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Contract Number</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Contract Number"
                    value={company.contract_no}
                    onChange={handleCompanyValue('contract_no')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Price</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Price"
                    value={company.price}
                    onChange={handleCompanyValue('price')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Cont</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Cont"
                    value={company.cont}
                    onChange={handleCompanyValue('cont')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Date</Form.Label>
                <Col>
                  <DropdownButton
                    variant="outline-secondary"
                    title={company.date_value}
                    id="input-group-dropdown-1"
                    drop="up"
                    onClick={e => handleDateSelect(e, searches)}
                  >
                    <Calendar
                      onChange={handleCompanyValue('date')}
                      value={company.date_select}
                    />
                  </DropdownButton>
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Container1</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Container1"
                    value={company.container_no1}
                    onChange={handleCompanyValue('container_no1')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Container1 Weight</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Container1 Weight"
                    value={company.container_no1_weight}
                    onChange={handleCompanyValue('container_no1_weight')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Container2</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Container1"
                    value={company.container_no2}
                    onChange={handleCompanyValue('container_no2')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Container2 Weight</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Container2 Weight"
                    value={company.container_no2_weight}
                    onChange={handleCompanyValue('container_no2_weight')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Container3</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Container1"
                    value={company.container_no3}
                    onChange={handleCompanyValue('container_no3')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Container3 Weight</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Container3 Weight"
                    value={company.container_no3_weight}
                    onChange={handleCompanyValue('container_no3_weight')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Container4</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Container4"
                    value={company.container_no4}
                    onChange={handleCompanyValue('container_no4')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Container4 Weight</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Container4 Weight"
                    value={company.container_no4_weight}
                    onChange={handleCompanyValue('container_no4_weight')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Container5</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Container5"
                    value={company.container_no5}
                    onChange={handleCompanyValue('container_no5')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Container5 Weight</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Container5 Weight"
                    value={company.container_no5_weight}
                    onChange={handleCompanyValue('container_no5_weight')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Total Weight</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Total Weight"
                    value={company.weight}
                    onChange={handleCompanyValue('weight')}
                  />
                </Col>
              </Form.Group>

              {extra_charges}

              {/* <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Other Expense</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Other Expense"
                    value={company.other_expense}
                    onChange={handleCompanyValue('other_expense')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Bank Expense</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Bank Expense"
                    value={company.bank_charges}
                    onChange={handleCompanyValue('bank_charges')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Clearing Charges</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Clearing Charges"
                    value={company.clearing_charges}
                    onChange={handleCompanyValue('clearing_charges')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Dollar Rate</Form.Label>
                <Col>
                  <Form.Control
                    type="number"
                    placeholder="Dollar Rate"
                    value={company.dollar_rate}
                    onChange={handleCompanyValue('dollar_rate')}
                  />
                </Col>
              </Form.Group>

              

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Shipper</Form.Label>
                <Col>
                  <Form.Control
                    type="text"
                    placeholder="Shipper"
                    value={company.shipper}
                    onChange={handleCompanyValue('shipper')}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formHorizontalCommodity">
                <Form.Label column>Arrival Date</Form.Label>
                <Col>
                  <DropdownButton
                    variant="outline-secondary"
                    title={company.arrival_date_value}
                    id="input-group-dropdown-1"
                    drop="up"
                  >
                    <Calendar
                      onChange={handleCompanyValue('arrival_date')}
                      value={company.arrival_date_select}
                    />
                  </DropdownButton>
                </Col>
              </Form.Group> */}
            </Form>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={handleAddClose}>
              Close
            </Button>
            <Button
              id="companyAdd"
              variant="primary"
              onClick={handleAddImport(company)}
            >
              {company.type} Import
            </Button>
          </Modal.Footer>
        </Modal>
      </FadeIn>
    </div>
    </div>
  );
}

export default ImportBody;
