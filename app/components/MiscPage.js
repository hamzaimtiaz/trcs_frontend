import React, { useState, Fragment } from 'react';
import {
  Container,
  Tab,
  Tabs,
  Row,
  Col,
  Card,
  Button,
  Form,
  DropdownButton,
  Dropdown,
  Table,
  Modal,
  Alert,
} from 'react-bootstrap';

import Calendar from 'react-calendar';

import 'react-calendar/dist/Calendar.css';
import FadeIn from 'react-fade-in';

function MiscBody({
  companies,
  commodities,
  allcommodities,
  banks,
  companySelectionHandler,
  addCompany,
  addCommodity,
  addBank,
}) {
  const [addCompanyShow, setaddCompanyShow] = useState(false);
  const [addCommodityShow, setaddCommodityShow] = useState(false);
  const [addBankShow, setaddBankShow] = useState(false);

  const [company, setCompany] = useState({
    id: 0,
    name: null,
    location: null,
    contactNo: null,
    email: null,
    type: 'Add',
  });

  const [commodity, setCommodity] = useState({
    id: 0,
    name: null,
    company: null,
    company_name: 'Company',
    type: 'Add',
  });

  const [bank, setBank] = useState({
    id: 0,
    name: null,
    location: null,
    type: 'Add',
  });

  let CompanyRows = <div> </div>;
  let CommodityRows = <div> </div>;
  let BankRows = <div> </div>;
  let CompanySelectionRows = <div> </div>;

  const handleAddCompanyClose = () => setaddCompanyShow(false);
  const handleAddCompanyShow = () => setaddCompanyShow(true);

  const handleAddCommodityClose = () => setaddCommodityShow(false);
  const handleAddCommodityShow = () => setaddCommodityShow(true);

  const handleAddBankClose = () => setaddBankShow(false);
  const handleAddBankShow = () => setaddBankShow(true);

  const handleAddCompany = company_data => event => {
    addCompany(company_data);
    handleAddCompanyClose();
  };

  const handleAddCommodity = commodity_data => event => {
    addCommodity(commodity_data);
    handleAddCommodityClose();
  };

  const handleAddBank = bank_data => event => {
    addBank(bank_data);
    handleAddBankClose();
  };

  const handleEditCompany = data => event => {
    setCompany({
      ...company,
      id: data.id,
      name: data.name,
      location: data.location,
      contactNo: data.contactNo,
      email: data.email,
      type: 'Update',
    });
    handleAddCompanyShow();
  };

  const handleEditCommodity = data => event => {
    setCommodity({
      ...commodity,
      id: data.id,
      name: data.name,
      company: data.company.id,
      company_name: data.company.name,
      type: 'Update',
    });
    handleAddCommodityShow();
  };

  const handleEditBank = data => event => {
    setBank({
      ...bank,
      id: data.id,
      name: data.name,
      location: data.location,
      type: 'Update',
    });
    handleAddBankShow();
  };

  const handleCompanyValue = parameter => event => {
    setCompany({
      ...company,
      [parameter]: event.target.value,
    });
  };

  const handleBankValue = parameter => event => {
    setBank({
      ...bank,
      [parameter]: event.target.value,
    });
  };

  const handleCommodityValue = parameter => event => {
    if (parameter === 'company') {
      setCommodity({
        ...commodity,
        [parameter]: event.split(',')[0],
        company_name: event.split(',')[1],
      });
    } else {
      setCommodity({
        ...commodity,
        [parameter]: event.target.value,
      });
    }
  };

  if (companies.length>0) {
    const count = 0;
    CompanyRows = companies.map((data, index) => (
      <tr>
        {/* <td>
          <Form.Check type="checkbox" />
        </td> */}
        <td>{index + 1}</td>
        <td>{data.name}</td>
        <td>{data.location}</td>
        <td>{data.contactNo}</td>
        <td>{data.email}</td>

        <td>
          <Button variant="secondary" onClick={handleEditCompany(data)}>
            Edit
          </Button>
        </td>
      </tr>
    ));
  }
  else
  {
    CompanyRows = <Alert variant='secondary' dismissible >
      No data found!
      </Alert>
  }

  if (banks.length>0) {
    const count = 0;
    BankRows = banks.map((data, index) => (
      <tr>
        {/* <td>
          <Form.Check type="checkbox" />
        </td> */}
        <td>{index + 1}</td>
        <td>{data.name}</td>
        <td>{data.location}</td>
        <td>
          <Button variant="secondary" onClick={handleEditBank(data)}>
            Edit
          </Button>
        </td>
      </tr>
    ));
  }
  else
  {
    BankRows = <Alert variant='secondary' dismissible >
      No data found!
      </Alert>
  }

  if (allcommodities.length>0) {
    CommodityRows = allcommodities.map((data, index) => (
      <tr>
        {/* <td>
          <Form.Check type="checkbox" />
        </td> */}
        <td>{index + 1}</td>
        <td>{data.name}</td>
        <td>{data.company.name}</td>
        <td>
          <Button variant="secondary" onClick={handleEditCommodity(data)}>
            Edit
          </Button>
        </td>
      </tr>
    ));
  }
  else
  {
    CommodityRows = <Alert variant='secondary' dismissible >
      No data found!
      </Alert>
  }

  if (companies) {
    CompanySelectionRows = companies.map(data => (
      <Dropdown.Item eventKey={new Array([data.id, data.name])}>
        {data.name}
      </Dropdown.Item>
    ));
  }

  return (
    <div>
      <FadeIn delay="100">
        <Form>
          <div>
            {' '}
            <Card>
              <Card.Header className="float-right">
                <Button
                  className="float-right"
                  variant="secondary"
                  style={{ margin: '5px' }}
                  onClick={handleAddCompanyShow}
                >
                  Add
                </Button>
                {''}
                <Card.Title>Companies</Card.Title>
              </Card.Header>
              <Card.Body>
                <Container>
                  <Table
                    bordered
                    hover
                    class="table-responsive"
                    style={{
                      // overflow: 'auto',
                      // display: 'block',
                      // 'table-layout': 'auto',
                      'white-space': 'nowrap',
                      height: '50px',
                      'overflow-y': 'scroll',
                    }}
                  >
                    <thead style={{position: 'sticky' , top: 0 ,'background-color': 'grey',color:'black'}}>
                      <tr>
                        {/* <th>&nbsp;</th> */}
                        <th>#</th>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Contact No</th>
                        <th>Email</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>{CompanyRows}</tbody>
                  </Table>
                </Container>
              </Card.Body>
            </Card>
          </div>
          <div>
            {' '}
            &nbsp;
            <Card>
              <Card.Header className="float-right">
                <Button
                  className="float-right"
                  variant="secondary"
                  style={{ margin: '5px' }}
                  onClick={handleAddBankShow}
                >
                  Add
                </Button>
                {''}
                <Card.Title>Banks</Card.Title>
              </Card.Header>
              <Card.Body>
                <Container>
                  <Table
                    bordered
                    hover
                    class="table-responsive"
                    style={{
                      // overflow: 'auto',
                      // display: 'block',
                      // 'table-layout': 'auto',
                      // 'white-space': 'nowrap',
                      height: '50px',
                      'overflow-y': 'scroll',
                    }}
                  >
                    <thead style={{position: 'sticky' , top: 0 ,'background-color': 'grey',color:'black'}}>
                      <tr>
                        {/* <th>&nbsp;</th> */}
                        <th>#</th>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>{BankRows}</tbody>
                  </Table>
                </Container>
              </Card.Body>
            </Card>
          </div>
          <div>
            {' '}
            &nbsp;
            <Card>
              <Card.Header className="float-right">
                <Button
                  className="float-right"
                  variant="secondary"
                  style={{ margin: '5px' }}
                  onClick={handleAddCommodityShow}
                >
                  Add
                </Button>
                {''}
                <Card.Title>Commodities</Card.Title>
              </Card.Header>
              <Card.Body>
                <Container>
                  <Table
                    bordered
                    hover
                    size="sm"
                    class="table-responsive"
                    style={{
                      // overflow: 'auto',
                      // display: 'table',
                      // 'table-layout': 'auto',
                      // 'white-space': 'nowrap',
                      // 'max-height': '1rem',
                      height: '200px',
                      'overflow-y': 'scroll',
                    }}
                  >
                    <thead style={{position: 'sticky' , top: 0 ,'background-color': 'grey',color:'black'}}>
                      <tr>
                        {/* <th>&nbsp;</th> */}
                        <th>#</th>
                        <th>Name</th>
                        <th>Company</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>{CommodityRows}</tbody>
                  </Table>
                </Container>
              </Card.Body>
            </Card>
          </div>
        </Form>

        <Modal show={addCompanyShow} onHide={handleAddCompanyClose}>
          <Modal.Header closeButton>
            <Modal.Title>{company.type} Company</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <FadeIn delay="100">
                <div>
                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Name</Form.Label>
                    <Col>
                      <Form.Control
                        type="text"
                        placeholder="Name"
                        value={company.name}
                        onChange={handleCompanyValue('name')}
                      />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Location</Form.Label>
                    <Col>
                      <Form.Control
                        type="text"
                        placeholder="Location"
                        value={company.location}
                        onChange={handleCompanyValue('location')}
                      />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Contact Number</Form.Label>
                    <Col>
                      <Form.Control
                        type="text"
                        placeholder="Contact Number"
                        value={company.contactNo}
                        onChange={handleCompanyValue('contactNo')}
                      />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Email</Form.Label>
                    <Col>
                      <Form.Control
                        type="text"
                        placeholder="Email"
                        value={company.email}
                        onChange={handleCompanyValue('email')}
                      />
                    </Col>
                  </Form.Group>
                </div>
              </FadeIn>
            </Form>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={handleAddCompanyClose}>
              Close
            </Button>
            <Button
              id="companyAdd"
              variant="primary"
              onClick={handleAddCompany(company)}
            >
              {company.type} Company
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={addCommodityShow} onHide={handleAddCommodityClose}>
          <Modal.Header closeButton>
            <Modal.Title>{commodity.type} Commodity</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <FadeIn delay="100">
                <div>
                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Name</Form.Label>
                    <Col>
                      <Form.Control
                        type="text"
                        placeholder="Name"
                        value={commodity.name}
                        onChange={handleCommodityValue('name')}
                      />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Company</Form.Label>
                    <Col>
                      <DropdownButton
                        variant="outline-secondary"
                        title={commodity.company_name}
                        id="input-group-dropdown-1"
                        onSelect={handleCommodityValue('company')}
                      >
                        {CompanySelectionRows}
                      </DropdownButton>
                    </Col>
                  </Form.Group>
                </div>
              </FadeIn>
            </Form>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={handleAddCommodityClose}>
              Close
            </Button>
            <Button
              id="companyAdd"
              variant="primary"
              onClick={handleAddCommodity(commodity)}
            >
              {commodity.type} Commodity
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={addBankShow} onHide={handleAddBankClose}>
          <Modal.Header closeButton>
            <Modal.Title>{bank.type} Bank</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <FadeIn delay="100">
                <div>
                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Name</Form.Label>
                    <Col>
                      <Form.Control
                        type="text"
                        placeholder="Name"
                        value={bank.name}
                        onChange={handleBankValue('name')}
                      />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="formHorizontalCommodity">
                    <Form.Label column>Location</Form.Label>
                    <Col>
                      <Form.Control
                        type="text"
                        placeholder="Location"
                        value={bank.location}
                        onChange={handleBankValue('location')}
                      />
                    </Col>
                  </Form.Group>
                </div>
              </FadeIn>
            </Form>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={handleAddBankClose}>
              Close
            </Button>
            <Button
              id="companyAdd"
              variant="primary"
              onClick={handleAddBank(bank)}
            >
              {bank.type} Bank
            </Button>
          </Modal.Footer>
        </Modal>
      </FadeIn>
    </div>
  );
}

export default MiscBody;
