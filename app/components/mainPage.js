// import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, Fragment } from 'react';
import {
  Container,
  Tab,
  Tabs,
  Row,
  Col,
  Card,
  Button,
  Form,
  DropdownButton,
  Dropdown,
  Table,
  Modal,
  Alert,
} from 'react-bootstrap';
import ImportBody from './ImportPage';
import PaymentBody from './PaymentsPage';
import MiscBody from './MiscPage';

function MainBody({
  company,
  banks,
  handleOnCompanySelected,
  commodity,
  handleOnCompanySearch,
  searchData,
  addImport,
  handleOnPaymentSearch,
  searchPaymentData,
  addPayment,
  allCommodity,
  addCompany,
  addCommodity,
  addBank,
  added,
}) {
  const emails_jsx = <div />;

  const [alertShow, setAlertShow] = useState(true);

  const handleAlertClose = () => setAlertShow(false);
  const handleAlertShow = () => setAlertShow(true);

  // if(added === true)
  // {
  //   console.log("here",added)
  //   // setAlertShow(true);
  // }
  return (
    <div>
      <nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#" style={{ 'display': 'inline-block',
    'text-align': 'center',
    'margin-left': '500px',
    'font-family': 'Arial, Helvetica, sans-serif',
    'font-size':'25px',
    'font-style':'bold'}}>
      {/* <img src={require("./logo.png")} width="30" height="30" alt=""/> */}
      Tariq Rehman Commission Shop
    </a>
  </nav>
      <Container>
      
      {added == true &&
      <Alert show={alertShow} variant='success' dismissible onClose={handleAlertClose}>Successfull!</Alert>
        // ? <Alert show={alertShow} variant='success' dismissible onClose={handleAlertClose}>Successfull!</Alert>
        // : <Alert show={alertShow} variant='danger' dismissible onClose={handleAlertClose}>Failed!</Alert>
      }
      {added == false &&
        <Alert show={alertShow} variant='danger' dismissible onClose={handleAlertClose}>Failed!</Alert>
      }
      
        <Tabs
          defaultActiveKey="imports"
          id="uncontrolled-tab-example"
          // transition={false}
        >
          <Tab eventKey="imports" title="Imports">
            <div>
              <Card>
                <Card.Body>
                  <ImportBody
                    companies={company}
                    commodities={commodity}
                    banks={banks}
                    companySelectionHandler={handleOnCompanySelected}
                    handleOnCompanySearch={handleOnCompanySearch}
                    searchData={searchData}
                    addImport={addImport}
                  />
                </Card.Body>
              </Card>
            </div>
          </Tab>
          <Tab eventKey="payments" title="Payments">
          <div>
              <Card>
                <Card.Body>
                  <PaymentBody
                    companies={company}
                    commodities={commodity}
                    banks={banks}
                    companySelectionHandler={handleOnCompanySelected}
                    handleOnPaymentSearch={handleOnPaymentSearch}
                    searchPaymentData={searchPaymentData}
                    addPayment={addPayment}
                  />
                </Card.Body>
              </Card>
            </div>
          </Tab>
          <Tab eventKey="misc" title="Misc">
          <div>
              <Card>
                <Card.Body>
                  <MiscBody
                    companies={company}
                    commodities={commodity}
                    allcommodities={allCommodity}
                    banks={banks}
                    companySelectionHandler={handleOnCompanySelected}
                    // handleOnCompanySearch={handleOnCompanySearch}
                    // searchData={searchData}
                    // addImport={addImport}
                    addCompany={addCompany}
                    addCommodity={addCommodity}
                    addBank={addBank}
                  />
                </Card.Body>
              </Card>
            </div>
          </Tab>
        </Tabs>
      </Container>
    </div>
  );
}
export default MainBody;
