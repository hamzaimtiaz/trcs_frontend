import React, { useState, Fragment } from 'react';
import {
  Container,
  Tab,
  Tabs,
  Row,
  Col,
  Card,
  Button,
  Form,
  DropdownButton,
  Dropdown,
  Table,
  Modal,
  Alert,
} from 'react-bootstrap';

import Calendar from 'react-calendar';

import 'react-calendar/dist/Calendar.css';
import FadeIn from 'react-fade-in';

function CompanyPrint({
  searchData,
}) {
  // const [addCompanyShow, setaddCompanyShow] = useState(false);
  // const [addCommodityShow, setaddCommodityShow] = useState(false);
  // const [addBankShow, setaddBankShow] = useState(false);

  
  // let CompanyRows = <div> </div>;
  // let CommodityRows = <div> </div>;
  // let BankRows = <div> </div>;
  // let CompanySelectionRows = <div> </div>;
  let DataRows = <div> </div>;

 
  if (searchData.length > 0) {
    const count = 0;
    DataRows = searchData.map((data, index) => (
      <tr>
        {/* <td>
          <Form.Check type="checkbox" />
        </td> */}
        <td>{index + 1}</td>
        <td>{data.company.name}</td>
        <td>{data.commodity.name}</td>
        <td>{data.bank.name}</td>
        <td>{data.transaction_type}</td>
        <td>{data.BL_number}</td>

        {data.transaction_type === 'Sale'
          ? <td>{data.price.toLocaleString()} PKR</td>
          : <td>{data.price.toLocaleString()} $</td>
        }
        <td>{data.cont}</td>
        <td>{data.date}</td>

        <td>{data.container_no1}</td>
        <td>{data.container_no1_weight.toLocaleString()}</td>
        <td>{data.container_no2}</td>
        <td>{data.container_no2_weight.toLocaleString()}</td>
        <td>{data.container_no3}</td>
        <td>{data.container_no3_weight.toLocaleString()}</td>
        <td>{data.container_no4}</td>
        <td>{data.container_no4_weight.toLocaleString()}</td>
        <td>{data.container_no5}</td>
        <td>{data.container_no5_weight.toLocaleString()}</td>

        <td>{data.other_expense.toLocaleString()} PKR</td>
        <td>{data.bank_charges.toLocaleString()} PKR</td>
        <td>{data.clearing_charges.toLocaleString()} PKR</td>
        <td>{data.dollar_rate}</td>

        {/* <td>{data.container_no}</td> */}
        <td>{data.transaction_number}</td>
        <td>{data.shipper}</td>
        <td>{data.arrival_date}</td>

        {data.transaction_type === 'Sale'
          ? <td>{data.invoice.toLocaleString()} PKR</td>
          : <td>{data.invoice.toLocaleString()} $</td>
        }
        <td>{data.amount.toLocaleString()} PKR</td>
        {data.prev_balance
          ? <td>{data.prev_balance.toLocaleString()} PKR</td>
          : <td>0 PKR</td>
        }

        <td>{data.balance.toLocaleString()} PKR</td>
        <td>{data.container1_weight_difference.toLocaleString()}</td>
        <td>{data.container2_weight_difference.toLocaleString()}</td>
        <td>{data.container3_weight_difference.toLocaleString()}</td>
        <td>{data.container4_weight_difference.toLocaleString()}</td>
        <td>{data.container5_weight_difference.toLocaleString()}</td>
        {/* <td>{data.price_difference}</td> */}
        
      </tr>
    ));
  }
 
  return (
    <div>
        <Form>
          <div>

          <Table
                    bordered
                    hover
                    size="xl"
                    class="table-responsive"
                    // style={{
                    //   overflow: 'auto',
                    //   display: 'block',
                    //   'table-layout': 'auto',
                    //   'white-space': 'nowrap',
                    //   height: '500px',
                    //   'overflow-y': 'scroll',
                    // }}
                  >
                    <thead
                      // style={{
                      //   position: 'sticky',
                      //   top: 0,
                      //   'background-color': 'grey',
                      //   color: 'black',
                      // }}
                    >
                      <tr>
                        {/* <th>&nbsp;</th> */}
                        <th>#</th>
                        <th>Company</th>
                        <th>Commodity</th>
                        <th>Bank</th>
                        <th>Transaction Type</th>
                        <th>BL Number</th>
                        <th>Price</th>
                        <th>Cont</th>
                        <th>Date</th>
                        <th>Container 1</th>
                        <th>Container 1 Weight</th>
                        <th>Container 2</th>
                        <th>Container 2 Weight</th>
                        <th>Container 3</th>
                        <th>Container 3 Weight</th>
                        <th>Container 4</th>
                        <th>Container 4 Weight</th>
                        <th>Container 5</th>
                        <th>Container 5 Weight</th>

                        <th>Other Expense</th>
                        <th>Bank Charges</th>
                        <th>Clearing Charges</th>
                        <th>Dollar Rate</th>

                        <th>Transaction Number</th>
                        <th>Shipper</th>
                        <th>Arrival Date</th>

                        <th>Invoice</th>
                        <th>Amount</th>
                        <th>Previous Balance</th>
                        <th>Balance</th>

                        <th>C1 Wt diff</th>
                        <th>C2 Wt diff</th>
                        <th>C3 Wt diff</th>
                        <th>C4 Wt diff</th>
                        <th>C5 Wt diff</th>

                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>{DataRows}</tbody>
                  </Table>
           </div>
           </Form>
    </div>
  );
}

export default CompanyPrint;
